package ph.gov.dost.region1.api.users.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created on 1/16/21
 * Created by Ryan Jan Borja
 * rrborja@region1.dost.gov.ph
 * ryanjan18@outlook.ph
 */

@Repository
@Transactional(readOnly = true)
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {

  // Recommended Approach: PSQL with Named Parameters
  @Query("SELECT s FROM Student s WHERE s.email = ?1")
  Optional<StudentEntity> findByEmail(String email);

  @Query("SELECT s FROM Student s WHERE s.firstName = :firstName AND s.age >= :age")
  List<StudentEntity> findByFirstNameEqualsAndAgeIsGreaterThanEqual(@Param("firstName") String firstName, @Param("age") Integer age);

  @Transactional
  @Modifying
  @Query("DELETE FROM Student u WHERE u.id = :id")
  int deleteStudentById(@Param("id") Long id);

  // Only use when your DB vendor is fixed
  @Query(value = "SELECT * FROM student WHERE first_name = ?1 AND age >= ?2", nativeQuery = true)
  List<StudentEntity> findByFirstNameEqualsAndAgeIsGreaterThanEqualNative(String firstName, Integer age);

}
