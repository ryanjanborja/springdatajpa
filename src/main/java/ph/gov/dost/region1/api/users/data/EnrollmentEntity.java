package ph.gov.dost.region1.api.users.data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created on 1/24/21
 * Created by Ryan Jan Borja
 * rrborja@region1.dost.gov.ph
 * ryanjan18@outlook.ph
 */

@Entity(name = "Enrollment")
@Table(name = "enrollment")
public class EnrollmentEntity {

  @EmbeddedId
  private EnrollmentId id;

  @ManyToOne
  @MapsId("studentId")
  @JoinColumn(
    name = "student_id",
    foreignKey = @ForeignKey(name = "enrollment_student_id_fk")
  )
  private StudentEntity student;

  @ManyToOne
  @MapsId("courseId")
  @JoinColumn(
    name = "course_id",
    foreignKey = @ForeignKey(name = "enrollment_course_id_fk")
  )
  private CourseEntity course;

  @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime createdAt;

  public EnrollmentEntity() { }

  public EnrollmentEntity(EnrollmentId id, StudentEntity student, CourseEntity course, LocalDateTime createdAt) {
    this.id = id;
    this.student = student;
    this.course = course;
    this.createdAt = createdAt;
  }

  public EnrollmentId getId() {
    return id;
  }

  public void setId(EnrollmentId id) {
    this.id = id;
  }

  public StudentEntity getStudent() {
    return student;
  }

  public void setStudent(StudentEntity student) {
    this.student = student;
  }

  public CourseEntity getCourse() {
    return course;
  }

  public void setCourse(CourseEntity course) {
    this.course = course;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public String toString() {
    return "EnrollmentEntity{" +
      "id=" + id +
      ", student=" + student +
      ", course=" + course +
      '}';
  }

}
