package ph.gov.dost.region1.api.users.data;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

/**
 * Created on 1/24/21
 * Created by Ryan Jan Borja
 * rrborja@region1.dost.gov.ph
 * ryanjan18@outlook.ph
 */

@Entity(name = "Course")
@Table(name = "course")
public class CourseEntity {

  @Id
  @SequenceGenerator(name = "course_sequence", sequenceName = "course_sequence", allocationSize = 1)
  @GeneratedValue(strategy = SEQUENCE, generator = "course_sequence")
  @Column(name = "id", updatable = false)
  private Long id;

  @Column(name = "name", nullable = false, columnDefinition = "TEXT")
  private String name;

  @Column(name = "department", nullable = false, columnDefinition = "TEXT")
  private String department;

  @OneToMany(mappedBy = "course", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
  private List<EnrollmentEntity> enrollments = new ArrayList<>();

  public CourseEntity() { }

  public CourseEntity(String name, String department) {
    this.name = name;
    this.department = department;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public List<EnrollmentEntity> getEnrollment() {
    return enrollments;
  }

  public void addEnrollment(EnrollmentEntity enrollment) {
    if (!enrollments.contains(enrollment)) {
      enrollments.add(enrollment);
    }
  }

  public void removeEnrollment(EnrollmentEntity enrollment) {
    enrollments.remove(enrollment);
  }

  @Override
  public String toString() {
    return "CourseEntity{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", department='" + department + '\'' +
      '}';
  }

}
