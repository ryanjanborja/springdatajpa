package ph.gov.dost.region1.api.users.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

/**
 * Created on 1/18/21
 * Created by Ryan Jan Borja
 * rrborja@region1.dost.gov.ph
 * ryanjan18@outlook.ph
 */

public interface StudentIdCardRepository extends CrudRepository<StudentIdCard, Long> {

  @Query("SELECT sic FROM StudentIdCard sic WHERE sic.cardNumber = :card_number")
  Optional <StudentIdCard> findByCardNumber(@Param("card_number")UUID card_number);

}
